import tw, { styled } from 'twin.macro';

const containerVariant = {
  large: tw`hidden md:block mx-4 mt-16 px-2 shadow-lg border-l border-b border-yellow-800 h-auto text-center`,
  mobile: tw`md:hidden mx-4 mt-16 px-2 shadow-lg border-l border-b border-yellow-300 h-auto text-center`,
};

const listContainerVariant = {
  large: tw`hidden md:flex w-full border-t border-b border-yellow-800 py-4 text-sm justify-between items-center text-left`,
  mobile: tw`flex md:hidden w-full border-t border-b border-yellow-300 py-2 text-sm justify-between items-center text-left`,
};

export const Container = styled.div(() => [
  ({ variant = 'mobile' }) => containerVariant[variant],
]);

export const ListContainer = styled.li(() => [
  ({ variant = 'mobile' }) => listContainerVariant[variant],
]);
