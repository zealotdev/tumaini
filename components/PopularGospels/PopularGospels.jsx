import { SiDeezer, SiAudiomack } from 'react-icons/si';
import { Title } from '../elements';
import { Container, ListContainer } from './PopularGospels.styled.components';

export default function PopularGospels() {
  return (
    <>
      <Container variant="large">
        <Title className="mb-8">Our Most Popular Gospel Musics</Title>
        <MusicList />
        <div className="mt-6 mb-4 flex justify-end space-x-4">
          <label className="underline text-right font-bold opacity-40">
            Listen to All on {'>'}
          </label>
          <div className="text-2xl flex space-x-4">
            <a
              href="https://www.deezer.com/search/tumaini%20shangilieni%20choir/track"
              target="_blank"
            >
              <SiDeezer />
            </a>
            <a href="https://audiomack.com/tumainishangilieni" target="_blank">
              <SiAudiomack />
            </a>
          </div>
        </div>
      </Container>
      <Container>
        <Title className="mb-4">Our Most Popular Gospel Musics</Title>
        <MusicList />
        <div className="mt-6 mb-4 flex justify-end space-x-4">
          <label className="underline text-right font-bold opacity-40">
            Listen to All on {'>'}
          </label>
          <div className="text-xl flex space-x-2">
            <a
              href="https://www.deezer.com/search/tumaini%20shangilieni%20choir/track"
              target="_blank"
            >
              <SiDeezer />
            </a>
            <a href="https://audiomack.com/tumainishangilieni" target="_blank">
              <SiAudiomack />
            </a>
          </div>
        </div>
      </Container>
    </>
  );
}

const MusicList = () => {
  return (
    <ul>
      <MusicItem
        title={'Ewe Mwanadamu'}
        album={'Ewe Mwanadamu'}
        audiomackURL="https://audiomack.com/tumainishangilieni/song/ewe-mwanadamu-1"
        deezerURL="https://deezer.page.link/7SQA52hNHAcULyjD7"
      />
      <MusicItem
        title={'Dunia hii'}
        album={'Ewe Mwanadamu'}
        audiomackURL="https://audiomack.com/tumainishangilieni/song/dunia-hii-2"
        deezerURL="https://deezer.page.link/vsS3y2DfV4UEeG1f9"
      />
      <MusicItem
        title={'Ewe Bwana Mungu Wangu'}
        album={'Ewe Mwanadamu'}
        audiomackURL="https://audiomack.com/tumainishangilieni/song/ewe-bwana-mungu-wangu"
        deezerURL="https://deezer.page.link/p8FXye3CmbPjPoHt7"
      />
      <MusicItem
        title={'Majibu ya Maombi'}
        album={'Ewe Mwanadamu'}
        audiomackURL="https://audiomack.com/tumainishangilieni/song/majibu-ya-maombi-1"
        deezerURL="https://deezer.page.link/iH2ENj97PhwwSrvLA"
      />
      <MusicItem
        title={'Tazama Anakuja'}
        album={'Ewe Mwanadamu'}
        audiomackURL="https://audiomack.com/tumainishangilieni/song/tazama-anakuja-1"
        deezerURL="https://deezer.page.link/fQNoPw4wkcSWYTT29"
      />
      <MusicItem
        title={'Ndugu Zangu'}
        album={'Ewe Mwanadamu'}
        audiomackURL="https://audiomack.com/tumainishangilieni/song/ndugu-zangu-1"
        deezerURL="https://deezer.page.link/XdSwyomjny6Kq5bM8"
      />
    </ul>
  );
};

const MusicItem = ({ title, album, audiomackURL, deezerURL }) => {
  return (
    <>
      <ListContainer variant="large">
        <div>
          <h5>{title}</h5>
          <span className="text-xs opacity-60">
            <b>Album:</b> {album}
          </span>
        </div>
        <div className="text-xs flex items-center">
          <label htmlFor="platform" className="mr-4 opacity-60">
            Listen on:
          </label>
          <div className="flex text-2xl space-x-2 text-white">
            <a href={deezerURL} target="_blank">
              <SiDeezer />
            </a>
            <a href={audiomackURL} target="_blank">
              <SiAudiomack />
            </a>
          </div>
        </div>
      </ListContainer>
      <ListContainer>
        <div>
          <h5>{title}</h5>
          <span className="text-xs opacity-60">
            <b>Album:</b> {album}
          </span>
        </div>
        <div className="text-xs flex items-center">
          <label htmlFor="platform" className="mr-4 opacity-60">
            Listen on:
          </label>
          <div className="flex text-xl space-x-2 text-yellow-900">
            <a href={deezerURL} target="_blank">
              <SiDeezer />
            </a>
            <a href={audiomackURL} target="_blank">
              <SiAudiomack />
            </a>
          </div>
        </div>
      </ListContainer>
    </>
  );
};
