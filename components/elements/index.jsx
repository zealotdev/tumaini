import tw, { styled } from 'twin.macro';

const titleVariant = {
  large: tw`text-3xl`,
  mobile: tw`text-2xl`,
};
export const Event = styled.div(() => [
  tw`mx-auto my-4`,
  ({ variant = 'mobile' }) => eventVariant[variant],
]);

export const Title = styled.h3(() => [
  `
  font-family: 'Lobster', cursive;
  position: relative;
  // text-align:center; 
`,
  ({ variant = 'mobile' }) => titleVariant[variant],
]);

export const Label = tw.label`
   text-xs opacity-60
`;
