import tw, { styled } from 'twin.macro';

// Navbar Container
export const NavbarContainer = styled.section`
  ${tw`py-4 px-4 lg:px-24 absolute flex justify-between items-center w-full z-10`}
`;

export const Logo = tw.a`p-1`;

// Navbar Links
export const NavLinkContainer = styled.ul`
  ${tw`flex text-yellow-50 md:space-x-2 lg:space-x-5 xl:space-x-10 font-light uppercase`}
`;
