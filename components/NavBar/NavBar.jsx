import Image from 'next/image';
import Link from 'next/link';

import {
  NavbarContainer,
  Logo,
  NavLinkContainer,
} from './NavBar.styled.components';

export default function NavBar() {
  return (
    <NavbarContainer>
      <Logo>
        <Image src="/logo.png" width={80} height={50} />
      </Logo>
      <NavLinkContainer>
        <li>
          <Link href="#">
            <a>Home</a>
          </Link>
        </li>
        <li>
          <Link href="#library">
            <a>Library</a>
          </Link>
        </li>
        <li>
          <Link href="#events">
            <a>Events</a>
          </Link>
        </li>
        <li>
          <Link href="#products">
            <a>Products</a>
          </Link>
        </li>
        <li>
          <Link href="#footer">
            <a>Contacts Us</a>
          </Link>
        </li>
      </NavLinkContainer>
    </NavbarContainer>
  );
}
