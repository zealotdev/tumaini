import tw, { styled } from 'twin.macro';

export const Container = tw.div`
  w-full
  bg-yellow-50
  px-4 py-4
  flex
  justify-between
  items-center
  fixed
  z-50
`;

export const NavLinks = styled.ul`
  ${tw`flex flex-col text-center text-2xl pt-20 space-y-2 z-40`}
`;

export const MenuIcon = styled.a`
  ${tw`
    text-yellow-900 p-2 rounded-full
  `}
  box-shadow: 1px 1px 9px rgba(146, 65, 13, 0.3);
`;
