import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';
import { HiOutlineMenuAlt3 } from 'react-icons/hi';
import { motion } from 'framer-motion';

import { Container, NavLinks, MenuIcon } from './MobileNav.styled.components';

const variants = {
  open: { opacity: 1, x: 0 },
  closed: { opacity: 0, x: '-100%' },
};

export default function MobileNav() {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <Container>
      <a>
        <Image src="/logo.png" width={50} height={35} />
      </a>
      <MenuIcon>
        <HiOutlineMenuAlt3
          size="30"
          onClick={() => setIsOpen((isOpen) => !isOpen)}
        />
      </MenuIcon>
      <motion.nav
        className="absolute bg-yellow-50 top-20 left-0 h-screen w-full text-yellow-900 border-t border-yellow-900"
        animate={isOpen ? 'open' : 'closed'}
        variants={variants}
      >
        <NavLinks>
          <li onClick={() => setIsOpen(false)}>
            <Link href="/">
              <a>HOME</a>
            </Link>
          </li>
          <li onClick={() => setIsOpen(false)}>
            <Link href="#library">
              <a>LIBRARY</a>
            </Link>
          </li>
          <li onClick={() => setIsOpen(false)}>
            <Link href="#events">
              <a>EVENTS</a>
            </Link>
          </li>
          <li onClick={() => setIsOpen(false)}>
            <Link href="#products">
              <a>PRODUCTS</a>
            </Link>
          </li>
          <li onClick={() => setIsOpen(false)}>
            <Link href="#footer">
              <a>CONTACTS</a>
            </Link>
          </li>
        </NavLinks>
      </motion.nav>
    </Container>
  );
}
