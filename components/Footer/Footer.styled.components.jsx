import tw, { styled } from 'twin.macro';

const containerVariant = {
  large: tw`hidden md:flex flex-col bg-yellow-50 text-yellow-900`,
  mobile: tw`md:hidden flex flex-col bg-yellow-900 text-yellow-50`,
};

const logoVarinat = {
  large: tw`p-2 my-2`,
  mobile: tw`p-2 bg-yellow-50 mx-auto w-2/12 rounded-full`,
};

export const Container = styled.div(() => [
  tw`text-center pt-16 pb-8 w-full`,
  ({ variant = 'mobile' }) => containerVariant[variant],
]);

export const Logo = styled.div(() => [
  tw`py-8`,
  ({ variant = 'mobile' }) => logoVarinat[variant],
]);
