import Image from 'next/image';
import Link from 'next/link';
import {
  RiFacebookFill,
  RiInstagramLine,
  RiTwitterFill,
  RiYoutubeFill,
} from 'react-icons/ri';

import { SiDeezer, SiAudiomack } from 'react-icons/si';

import { Container, Logo } from './Footer.styled.components';

export default function Footer() {
  return (
    <footer id="footer">
      <Container>
        <Logo>
          <Image src="/logo.png" height={40} width={50} />
        </Logo>
        <div className="w-7/12 mx-auto leading-3 mt-4">
          <span className="text-xs opacity-60 ">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Non nam
            sint dicta explicabo unde eligendi?
          </span>
        </div>
        <ul className="flex flex-col text-yellow-50 uppercase mt-4">
          <li>
            <Link href="#">
              <a>Home</a>
            </Link>
          </li>
          <li>
            <Link href="#library">
              <a>Library</a>
            </Link>
          </li>
          <li>
            <Link href="#events">
              <a>Events</a>
            </Link>
          </li>
          <li>
            <Link href="#products">
              <a>Products</a>
            </Link>
          </li>
          <li>
            <Link href="#footer">
              <a>Contacts Us</a>
            </Link>
          </li>
        </ul>
        <div className="flex mx-auto mt-6 space-x-8 text-xl">
          <a
            href="https://facebook.com/tumainishangilienchoir/"
            target="_blank"
          >
            <RiFacebookFill />
          </a>
          <a
            href="https://www.instagram.com/tumainishangilienichoir/"
            target="_blank"
          >
            <RiInstagramLine />
          </a>
          <a
            href="https://www.youtube.com/channel/UCgsqklKJeQ5Talp2bibZoHg"
            target="_blank"
          >
            <RiYoutubeFill />
          </a>
          <a
            href="https://www.deezer.com/search/tumaini%20shangilieni%20choir/track"
            target="_blank"
          >
            <SiDeezer />
          </a>
          <a href="https://audiomack.com/tumainishangilieni" target="_blank">
            <SiAudiomack />
          </a>
        </div>
      </Container>
      <Container variant="large">
        <Logo variant="large">
          <Image src="/logo.png" height={60} width={80} />
        </Logo>
        <div className="w-4/12 mx-auto leading-3 mt-4">
          <span className="text-sm opacity-60 ">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Non nam
            sint dicta explicabo unde eligendi?
          </span>
        </div>
        <ul className="flex text-yellow-900 uppercase mt-8 justify-center space-x-4">
          <li>
            <Link href="#">
              <a>Home</a>
            </Link>
          </li>
          <li>
            <Link href="#library">
              <a>Library</a>
            </Link>
          </li>
          <li>
            <Link href="#events">
              <a>Events</a>
            </Link>
          </li>
          <li>
            <Link href="#products">
              <a>Products</a>
            </Link>
          </li>
          <li>
            <Link href="#footer">
              <a>Contacts Us</a>
            </Link>
          </li>
        </ul>
        <div className="flex mx-auto mt-6 space-x-8 text-2xl">
          <a
            href="https://facebook.com/tumainishangilienchoir/"
            target="_blank"
          >
            <RiFacebookFill />
          </a>
          <a
            href="https://www.instagram.com/tumainishangilienichoir/"
            target="_blank"
          >
            <RiInstagramLine />
          </a>
          <a
            href="https://www.youtube.com/channel/UCgsqklKJeQ5Talp2bibZoHg"
            target="_blank"
          >
            <RiYoutubeFill />
          </a>
          <a
            href="https://www.deezer.com/search/tumaini%20shangilieni%20choir/track"
            target="_blank"
          >
            <SiDeezer />
          </a>
          <a href="https://audiomack.com/tumainishangilieni" target="_blank">
            <SiAudiomack />
          </a>
        </div>
      </Container>
    </footer>
  );
}
