import { Container, Event } from './Events.styled.components';
import { Title, Label } from './../elements';

export default function Events() {
  return (
    <div id="events">
      <Container>
        <Title>Upcoming Events</Title>
        <Label>Catch-up with us on upcoming event</Label>
        <Event>
          <p>Oops!!</p>
          <h4>No Confirmed Event Yet</h4>
        </Event>
      </Container>
      <Container variant="large">
        <Title variant="large">Upcoming Events</Title>
        <Label>Catch-up with us on upcoming event</Label>
        <Event variant="large">
          <span>Oops!!</span>
          <h4>No Confirmed Event Yet</h4>
        </Event>
      </Container>
    </div>
  );
}
