import tw, { styled } from 'twin.macro';

const containerVariants = {
  large: tw`hidden md:block text-yellow-900 bg-yellow-50 text-center pt-16`,
  mobile: tw`md:hidden text-yellow-50 bg-yellow-900 text-center pt-20`,
};

export const Container = styled.div(() => [
  tw`pb-10`,
  ({ variant = 'mobile' }) => containerVariants[variant],
]);

const eventVariant = {
  large: tw`hidden md:flex md:flex-col justify-center bg-yellow-900 text-yellow-50 h-44 w-3/6`,
  mobile: tw`flex flex-col justify-center md:hidden  text-yellow-900 bg-yellow-50 h-28 w-5/6`,
};

export const Event = styled.div(() => [
  tw`mx-auto my-4`,
  ({ variant = 'mobile' }) => eventVariant[variant],
]);
