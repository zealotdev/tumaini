import tw, { styled } from 'twin.macro';

const libraryVariants = {
  large: tw`hidden md:block bg-yellow-900 text-yellow-50 w-full px-24 pt-10`,
  mobile: tw`md:hidden bg-yellow-50 text-yellow-900 w-full pt-20 text-center`,
};

export const LibraryContainer = styled.div(() => [
  tw`pb-10`,
  ({ variant = 'mobile' }) => libraryVariants[variant],
]);
