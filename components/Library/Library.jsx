import tw, { css } from 'twin.macro';
import Link from 'next/link';
import { LibraryContainer } from './Library.styled.components';
import { Card, PopularGospels } from './../';
import { Title, Label } from '../elements';

export const imageList = [
  {
    id: 1,
    title: 'Lorem, ipsum dolor',
    desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati iusto perferendis optio sint porro nulla harum quisquam quaerat aperiam qui?',
    imagePath:
      'https://res.cloudinary.com/zealotdevo/image/upload/v1633683524/tumaini/images/image-1_ld8wo5.jpg',
  },
  {
    id: 2,
    title: 'Lorem, ipsum dolor',
    desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati iusto perferendis optio sint porro nulla harum quisquam quaerat aperiam qui?',
    imagePath:
      'https://res.cloudinary.com/zealotdevo/image/upload/v1633683465/tumaini/images/image-2_ipvakt.jpg',
  },
  {
    id: 3,
    title: 'Lorem, ipsum dolor',
    desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati iusto perferendis optio sint porro nulla harum quisquam quaerat aperiam qui?',
    imagePath:
      'https://res.cloudinary.com/zealotdevo/image/upload/v1633683447/tumaini/images/image-3_twajbx.jpg',
  },
  {
    id: 4,
    title: 'Lorem, ipsum dolor',
    desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati iusto perferendis optio sint porro nulla harum quisquam quaerat aperiam qui?',
    imagePath:
      'https://res.cloudinary.com/zealotdevo/image/upload/v1633683439/tumaini/images/image-4_rybets.jpg',
  },
  {
    id: 5,
    title: 'Lorem, ipsum dolor',
    desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati iusto perferendis optio sint porro nulla harum quisquam quaerat aperiam qui?',
    imagePath:
      'https://res.cloudinary.com/zealotdevo/image/upload/v1633683405/tumaini/images/image-5_n2tpop.jpg',
  },
  {
    id: 6,
    title: 'Lorem, ipsum dolor',
    desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati iusto perferendis optio sint porro nulla harum quisquam quaerat aperiam qui?',
    imagePath:
      'https://res.cloudinary.com/zealotdevo/image/upload/v1633683515/tumaini/images/image-6_a2zsfi.jpg',
  },
  {
    id: 7,
    title: 'Lorem, ipsum dolor',
    desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati iusto perferendis optio sint porro nulla harum quisquam quaerat aperiam qui?',
    imagePath:
      'https://res.cloudinary.com/zealotdevo/image/upload/v1633683481/tumaini/images/image-7_cwrbbr.jpg',
  },
];
export default function Library() {
  return (
    <div id="library">
      <LibraryContainer variant="large">
        <div className="flex">
          <div className="w-6/12">
            <div>
              <Card
                key={imageList[0].id}
                imageId={imageList[0].id}
                title={imageList[0].title}
                description={imageList[0].desc}
                imagePath={imageList[0].imagePath}
              />
            </div>
            <div className="w-10/12 space-x-2">
              <CardList
                imageList={imageList.slice(1, 3)}
                orientation="horizontal"
              />
              <CardList
                imageList={imageList.slice(5, 7)}
                orientation="horizontal"
              />
            </div>
          </div>
          <div className="w-4/12 justify-self-start">
            <div>
              <span
                css={css`
                  ${tw`relative text-xs left-12`}

                  :before {
                    content: '';
                    position: absolute;
                    left: -45px;
                    top: 50%;
                    border: none;
                    width: 2rem;
                    height: 2px;
                    background-color: #fefbeb;
                    border-radius: 2px;
                  }
                `}
              >
                Choir Celebration Concert - 21<sup>st</sup>, Nov,2020
              </span>
              <Title
                css={css`
                  font-size: 2.4rem;
                  margin-top: 0.4rem;
                `}
              >
                On Our Last Event
              </Title>
            </div>
            <div className="">
              <CardList imageList={imageList.slice(3, 5)} />
            </div>
            <Link href="/gallery">
              <a className="underline text-yellow-50 mt-6 text-center">
                Explore our Gallery {'>'}
              </a>
            </Link>
          </div>
        </div>

        <PopularGospels variant="large" />
      </LibraryContainer>
      <LibraryContainer>
        <Title>On Our Last Event</Title>
        <div className="w-4/5">
          <span
            css={css`
              ${tw`relative text-xs left-14`}

              :before {
                content: '';
                position: absolute;
                left: -45px;
                top: 50%;
                border: none;
                width: 2rem;
                height: 2px;
                background-color: #78350f;
                border-radius: 2px;
              }
            `}
          >
            Choir Celebration Concert - 21<sup>st</sup>, Nov,2020
          </span>
        </div>
        {/* Gallery */}
        <CardList imageList={imageList} />
        <Link href="/gallery">
          <a className="underline text-yellow-900 mt-6 text-center">
            Explore our Gallery {'>'}
          </a>
        </Link>
        {/* Popular Gospels */}
        <PopularGospels />
      </LibraryContainer>
    </div>
  );
}

const CardList = ({ imageList, orientation = 'vertical' }) => {
  return (
    <div
      className={
        orientation == 'vertical' ? 'text-center' : 'text-center flex space-x-4'
      }
    >
      {imageList.map((image) => (
        <Card
          key={image.id}
          imageId={image.id}
          title={image.title}
          description={image.desc}
          imagePath={image.imagePath}
        />
      ))}
    </div>
  );
};
