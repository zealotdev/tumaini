import ImageContainer from './ImageContainer';
import Title from './Title';

// const CardContainer
export default function Card({ imageId, title, description, imagePath }) {
  return (
    <div>
      <ImageContainer
        imageId={imageId}
        description={description}
        imagePath={imagePath}
      >
        <Title title={title} />
      </ImageContainer>
    </div>
  );
}
