import tw from 'twin.macro';

const DescriptionContainer = tw.div`
  absolute bottom-2 text-yellow-50 text-sm text-justify bg-yellow-900 bg-opacity-40 rounded-b-xl w-full p-4
`;
export default function DescriptionPlaceHolder({ desc }) {
  return (
    <DescriptionContainer>
      <p>{desc}</p>
    </DescriptionContainer>
  );
}
