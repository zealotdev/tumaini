import tw from 'twin.macro';

const TitleContainer = tw.div`
  absolute top-4 left-4 text-yellow-50 text-2xl
`;
export default function Title({ title }) {
  return (
    <TitleContainer>
      <h4>{title}</h4>
    </TitleContainer>
  );
}
