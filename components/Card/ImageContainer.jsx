import Image from 'next/image';
import { useState } from 'react';
import { motion } from 'framer-motion';

import DescriptionPlaceholder from './DescriptionPlaceholder';

export default function ImageContainer({
  imageId,
  description,
  children,
  imagePath,
}) {
  const [showDesc, setShowDesc] = useState(false);

  return (
    <motion.div
      className="w-full mx-auto relative my-2"
      whileHover={{ scale: 1.1 }}
      whileTap={{ scale: 0.9 }}
      onClick={() => setShowDesc(!showDesc)}
    >
      <Image
        src={imagePath}
        alt={description}
        width={500}
        height={300}
        className="rounded-xl shadow-lg"
      />
      {children}
      {showDesc && <DescriptionPlaceholder desc={description} />}
    </motion.div>
  );
}
