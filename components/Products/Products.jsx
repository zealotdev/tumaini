import Image from 'next/image';
import { motion } from 'framer-motion';
import tw, { css } from 'twin.macro';
import { useState } from 'react';
import { Title, Label } from './../elements';
import { Container, Switcher, Button } from './Products.styled.components';

export default function Products() {
  const [category, setCategory] = useState('casual');

  const handleClick = () => {
    if (category == 'casual') setCategory('audio/video');
    if (category == 'audio/video') setCategory('casual');
  };
  return (
    <div id="products">
      <Container>
        <Title>Our Products</Title>
        <Label>Support Our Service By Buying Our Products</Label>
        <Switcher onClick={() => handleClick()}>
          <Button active={category == 'casual' ? true : false}>Casual</Button>
          <Button active={category == 'audio/video' ? true : false}>
            Audio/Videos
          </Button>
        </Switcher>
        <ProductDisplay category={category} />
      </Container>
      <Container variant="large">
        <Title>Our Products</Title>
        <Label>Support Our Service By Buying Our Products</Label>
        <Switcher variant="large" onClick={() => handleClick()}>
          <Button active={category == 'casual' ? true : false}>Casual</Button>
          <Button active={category == 'audio/video' ? true : false}>
            Audio/Videos
          </Button>
        </Switcher>
        <ProductDisplay category={category} />
      </Container>
    </div>
  );
}

const ProductDisplay = ({ category }) => {
  const shop = [
    {
      category: 'casual',
      products: [
        {
          id: 1,
          name: 'T-shirt',
          size: 'S, M, L, XL, XXL',
          price: '19500',
          imagePath:
            'https://res.cloudinary.com/zealotdevo/image/upload/v1633683439/tumaini/images/products/Tshirt_dfo5li.jpg',
          desc: 'Enjoy a first grade T-shirt with TUMAINI SHANGILIENI CHOIR logo on it',
        },
        {
          id: 2,
          name: 'Cap',
          size: 'Free Size',
          price: '9500',
          imagePath:
            'https://res.cloudinary.com/zealotdevo/image/upload/v1633683397/tumaini/images/products/cap_ks1tej.jpg',
          desc: 'Enjoy a quality Cap with TUMAINI SHANGILIENI CHOIR logo on it',
        },
        {
          id: 3,
          name: 'Mug',
          size: 'Tea Cup',
          price: '4000',
          imagePath:
            'https://res.cloudinary.com/zealotdevo/image/upload/v1633683402/tumaini/images/products/Mugs_lpbzfq.jpg',
          desc: 'Drink your favorite drink using TUMAINI SHANGILIENI CHOIR branded Mug',
        },
      ],
    },
    {
      category: 'audio/video',
      products: [
        {
          id: 1,
          name: 'Pendrive',
          size: '8 GB',
          price: '18000',
          imagePath:
            'https://res.cloudinary.com/zealotdevo/image/upload/v1633683397/tumaini/images/products/Pendrive_zch2lu.jpg',
          desc: 'Enjoy our best collection of gospel videos in a pendrive',
        },
        {
          id: 2,
          name: 'CD',
          size: '4 GB',
          price: '5000',
          imagePath:
            'https://res.cloudinary.com/zealotdevo/image/upload/v1633683441/tumaini/images/products/cd_mjmuj8.jpg',
          desc: 'Enjoy our best collection of gospel videos in a CD',
        },
      ],
    },
  ];

  const productsOne = shop.find((stock) => stock.category == 'casual').products;

  const productsTwo = shop.find(
    (stock) => stock.category == 'audio/video'
  ).products;

  const [selectedOne, setSelectedOne] = useState(
    shop.find((stock) => stock.category == 'casual').products[0]
  );
  const [selectedTwo, setSelectedTwo] = useState(
    shop.find((stock) => stock.category == 'audio/video').products[0]
  );

  return (
    <div className="flex flex-col my-4 w-10/12 mx-auto">
      {category == 'casual' && (
        <>
          <div className="flex flex-col-reverse md:flex-row justify-evenly space-y-8 md:space-x-8">
            <motion.button
              whileHover={{ scale: 1.1 }}
              whileTap={{ scale: 0.9 }}
              className="md:hidden bg-yellow-900 text-yellow-50 px-6 py-2 font-bold self-center mt-4"
            >
              Buy Now
            </motion.button>
            <div>
              <Image src={selectedOne.imagePath} height={350} width={400} />
            </div>
            <div className="flex flex-col justify-between">
              <div className="flex flex-col items-start text-yellow-900 md:text-yellow-50 space-y-4">
                <span className="text-2xl md:text-4xl font-serif capitalize">
                  {selectedOne.name}
                </span>
                <span className="text-lg md:text-xl font-light">
                  <span className="uppercase font-mono">Size:</span>{' '}
                  {selectedOne.size}
                </span>
                <span className="text-2xl md:text-4xl font-light">
                  <span className="text-lg md:text-xl uppercase font-mono">
                    Price:
                  </span>
                  TZS
                  {selectedOne.price}/=
                </span>
                <span className="opacity-60 w-10/12 text-left text-sm tracking-widest">
                  {selectedOne.desc}
                </span>
              </div>
              <motion.button
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
                className="hidden md:block bg-yellow-50 text-yellow-900 px-6 py-2 font-bold self-start"
              >
                Buy Now
              </motion.button>
            </div>
          </div>
          <div className="flex mt-8 bg-white p-2 w-12/12 md:w-11/12 mx-auto">
            <div className="bg-yellow-900 px-6 flex items-center underline font-mono text-xs md:text-sm text-yellow-50">
              Other Products
            </div>

            <div className="overflow-x-scroll flex justify-center mx-auto space-x-4">
              <div
                className="flex flex-col content-center cursor-pointer hover:bg-gray-300 duration-700"
                onClick={() =>
                  setSelectedOne(productsOne.find((product) => product.id == 2))
                }
              >
                <Image
                  src="https://res.cloudinary.com/zealotdevo/image/upload/v1633683397/tumaini/images/products/cap_ks1tej.jpg"
                  height={180}
                  width={200}
                />
                <span className="text-yellow-900 underline font-bold text-xs md:text-sm">
                  Cap
                </span>
              </div>
              <div
                className="flex flex-col content-center cursor-pointer hover:bg-gray-300 duration-700"
                onClick={() =>
                  setSelectedOne(productsOne.find((product) => product.id == 1))
                }
              >
                <Image
                  src="https://res.cloudinary.com/zealotdevo/image/upload/v1633683439/tumaini/images/products/Tshirt_dfo5li.jpg"
                  height={180}
                  width={200}
                />
                <span className="text-yellow-900 underline font-bold text-xs md:text-sm">
                  Tshirt
                </span>
              </div>
              <div
                className="flex flex-col content-center cursor-pointer hover:bg-gray-300 duration-700"
                onClick={() =>
                  setSelectedOne(productsOne.find((product) => product.id == 3))
                }
              >
                <Image
                  src="https://res.cloudinary.com/zealotdevo/image/upload/v1633683402/tumaini/images/products/Mugs_lpbzfq.jpg"
                  height={180}
                  width={200}
                />
                <span className="text-yellow-900 underline font-bold text-xs md:text-sm">
                  Mug
                </span>
              </div>
            </div>
          </div>
        </>
      )}
      {category == 'audio/video' && (
        <>
          <div className="flex flex-col-reverse md:flex-row justify-evenly space-y-8 md:space-x-8">
            <motion.button
              whileHover={{ scale: 1.1 }}
              whileTap={{ scale: 0.9 }}
              className="md:hidden bg-yellow-900 text-yellow-50 px-6 py-2 font-bold self-center mt-4"
            >
              Buy Now
            </motion.button>
            <div>
              <Image src={selectedTwo.imagePath} height={350} width={400} />
            </div>
            <div className="flex flex-col justify-between">
              <div className="flex flex-col items-start text-yellow-900 md:text-yellow-50 space-y-4">
                <span className="text-2xl md:text-4xl font-serif capitalize">
                  {selectedTwo.name}
                </span>
                <span className="text-lg md:text-xl font-light">
                  <span className="uppercase font-mono">Size:</span>{' '}
                  {selectedTwo.size}
                </span>
                <span className="text-2xl md:text-4xl font-light">
                  <span className="text-lg md:text-xl uppercase font-mono">
                    Price:
                  </span>
                  TZS
                  {selectedTwo.price}/=
                </span>
                <span className="opacity-60 w-10/12 text-left text-sm tracking-widest">
                  {selectedTwo.desc}
                </span>
              </div>
              <motion.button
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
                className="hidden md:block bg-yellow-50 text-yellow-900 px-6 py-2 font-bold self-start"
              >
                Buy Now
              </motion.button>
            </div>
          </div>
          <div className="flex mt-8 bg-white p-2 w-12/12 md:w-11/12 mx-auto">
            <div className="bg-yellow-900 px-6 flex items-center underline font-mono text-xs md:text-sm text-yellow-50">
              Other Products
            </div>

            <div className="overflow-x-scroll flex justify-center mx-auto space-x-4">
              <div
                className="flex flex-col content-center cursor-pointer hover:bg-gray-300 duration-700"
                onClick={() =>
                  setSelectedTwo(productsTwo.find((product) => product.id == 2))
                }
              >
                <Image
                  src="https://res.cloudinary.com/zealotdevo/image/upload/v1633683441/tumaini/images/products/cd_mjmuj8.jpg"
                  height={180}
                  width={200}
                />
                <span className="text-yellow-900 underline font-bold text-xs md:text-sm">
                  CD
                </span>
              </div>
              <div
                className="flex flex-col content-center cursor-pointer hover:bg-gray-300 duration-700"
                onClick={() =>
                  setSelectedTwo(productsTwo.find((product) => product.id == 1))
                }
              >
                <Image
                  src="https://res.cloudinary.com/zealotdevo/image/upload/v1633683397/tumaini/images/products/Pendrive_zch2lu.jpg"
                  height={180}
                  width={200}
                />
                <span className="text-yellow-900 underline font-bold text-xs md:text-sm">
                  Pendrive
                </span>
              </div>
            </div>
          </div>
        </>
      )}
    </div>
  );
};
