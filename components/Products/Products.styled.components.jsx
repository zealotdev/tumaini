import tw, { styled } from 'twin.macro';

const containerVariant = {
  large: tw`hidden md:block bg-yellow-900 text-yellow-50 py-16`,
  mobile: tw`md:hidden bg-yellow-50 text-yellow-900 py-20`,
};

const switcherVariant = {
  large: tw`bg-yellow-100 w-2/12`,
  mobile: tw`bg-yellow-100 w-6/12 text-xs`,
};

const buttonVariant = {
  true: tw`bg-yellow-900 text-yellow-50`,
  false: tw`bg-transparent`,
};

export const Container = styled.div(() => [
  tw`text-center `,
  ({ variant = 'mobile' }) => containerVariant[variant],
]);

export const Switcher = styled.div(() => [
  tw`p-1 text-sm mx-auto my-4 rounded-xl flex justify-evenly`,
  ({ variant = 'mobile' }) => switcherVariant[variant],
]);

export const Button = styled.button(() => [
  tw`text-yellow-900 py-1 px-2 w-6/12 rounded-xl`,
  ({ active = false }) => buttonVariant[active],
]);
