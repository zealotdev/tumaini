import tw, { css } from 'twin.macro';
import { Title } from '../elements';

import {
  ContentContainer,
  MobileContentContainer,
  HintContainer,
} from './Hint.styled.components';

export default function Hint() {
  return (
    <div id="hint">
      <HintContainer variant="large">
        <ContentContainer>
          <Title
            variant="large"
            css={css`
              ${tw`mb-4`}
              :before {
                content: '';
                position: absolute;
                right: -8%;
                top: 50%;
                border: none;
                width: 4rem;
                height: 6px;
                background-color: #78350f;
                border-radius: 4px;
              }

              :after {
                content: '';
                position: absolute;
                left: -8%;
                top: 50%;
                border: none;
                width: 4rem;
                height: 6px;
                background-color: #78350f;
                border-radius: 4px;
              }
            `}
          >
            From His Own Words
          </Title>
          <p className="opacity-60">
            Let the word of Christ dwell in you richly, teaching and admonishing
            one another in all wisdom, singing psalms and hymns and spiritual
            songs, with thankfulness in your hearts to God. (Colossians 3:16)
          </p>
        </ContentContainer>
      </HintContainer>
      <HintContainer>
        <MobileContentContainer>
          <Title
            css={css`
              ${tw`mb-4`}
              :before {
                content: '';
                position: absolute;
                right: 8%;
                top: 50%;
                border: none;
                width: 2rem;
                height: 4px;
                background-color: #fefbeb;
                border-radius: 4px;
              }

              :after {
                content: '';
                position: absolute;
                left: 8%;
                top: 50%;
                border: none;
                width: 2rem;
                height: 4px;
                background-color: #fefbeb;
                border-radius: 4px;
              }
            `}
          >
            From His Own Words
          </Title>
          <p className="opacity-60">
            Let the word of Christ dwell in you richly, teaching and admonishing
            one another in all wisdom, singing psalms and hymns and spiritual
            songs, with thankfulness in your hearts to God. (Colossians 3:16)
          </p>
        </MobileContentContainer>
      </HintContainer>
    </div>
  );
}
