import tw, { styled } from 'twin.macro';

const hintsVariants = {
  large: tw`hidden md:block text-yellow-900 bg-yellow-50 px-24 py-20 xl:py-28`,
  mobile: tw`md:hidden bg-yellow-900 text-yellow-50 px-4 py-28`,
};

export const HintContainer = styled.div(() => [
  ({ variant = 'mobile' }) => hintsVariants[variant],
]);

export const ContentContainer = tw.div`
  w-8/12 xl:w-5/12 mx-auto text-center
`;
export const MobileContentContainer = tw.div`
 mx-auto text-center
`;
