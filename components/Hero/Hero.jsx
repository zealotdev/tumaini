import { motion } from 'framer-motion';
import { css } from 'twin.macro';
import Image from 'next/image';
import { NavBar } from '..';
import { MobileNav } from '..';
import { Title } from '../elements';

import {
  MainContainer,
  LargeScreenContainer,
  HeroContentContainer,
  LargeScreenHeroCTAContainer,
  CoverImage,
  Player,
  SmallScreenContainer,
  SmallScreenHeroCTAContainer,
  MobilePlayerContainer,
  MobileMusicPlayer,
} from './Hero.styled.components';

export default function Hero() {
  return (
    <MainContainer id="hero">
      <LargeScreenContainer>
        <NavBar />
        <HeroContentContainer>
          <LargeScreenHeroCTAContainer>
            <div
              className="space-y-2"
              css={css`
                font-family: 'Lobster', cursive;
              `}
            >
              <motion.h1
                animate={{ fontSize: `80px` }}
                className="text-5xl tracking-widest"
              >
                Gospel
              </motion.h1>
              <motion.h2
                animate={{ fontSize: `30px` }}
                transition={{ delay: 0.5 }}
                className="text-2xl tracking-widest"
              >
                The Breath of Christians
              </motion.h2>
            </div>
            <p className="mt-8 opacity-60">
              Watch and Listen to our beautiful collection of concert snippets
              and gospel videos on our YouTube Channel now.
            </p>
            <motion.button
              className="bg-yellow-900 text-yellow-50 py-2 px-5 uppercase rounded-full mt-4"
              whileHover={{ scale: 1.1 }}
              whileTap={{ scale: 0.9 }}
            >
              youtube
            </motion.button>
          </LargeScreenHeroCTAContainer>
          <div className="bg-yellow-900 w-full pt-32 relative z-0">
            <CoverImage
              css={css`
                :before {
                  content: '';
                  position: absolute;
                  right: -20px;
                  top: -20px;
                  border: none;
                  border-top: 2px solid #fefbeb;
                  border-right: 2px solid #fefbeb;
                  border-radius: 4px;
                  height: 4rem;
                  width: 4rem;
                }

                :after {
                  content: '';
                  position: absolute;
                  left: -20px;
                  bottom: -20px;
                  border: none;
                  border-bottom: 2px solid #fefbeb;
                  border-left: 2px solid #fefbeb;
                  border-radius: 4px;
                  height: 4rem;
                  width: 4rem;
                }
              `}
            >
              <Image
                src="https://res.cloudinary.com/zealotdevo/image/upload/v1633683465/tumaini/images/image-2_ipvakt.jpg"
                height={300}
                width={320}
              />
            </CoverImage>
            <Player>
              <Title variant="large" className="text-yellow-50">
                Latest Album
              </Title>
              <iframe
                src="https://audiomack.com/embed/album/tumainishangilieni/ewe-mwanadamu?background=0&color=78350F"
                scrolling="no"
                width="100%"
                height="400"
                scrollbars="no"
                frameBorder="0"
              ></iframe>
            </Player>
          </div>
        </HeroContentContainer>
      </LargeScreenContainer>
      <SmallScreenContainer>
        <MobileNav />
        <SmallScreenHeroCTAContainer>
          <div
            className="space-y-8"
            css={css`
              font-family: 'Lobster', cursive;
            `}
          >
            <motion.h1
              animate={{ fontSize: `70px` }}
              className="text-4xl tracking-widest"
            >
              Gospel
            </motion.h1>
            <motion.h2
              animate={{ fontSize: `26px` }}
              transition={{ delay: 0.5 }}
              className="text-xl tracking-widest"
            >
              The Breath of Christians
            </motion.h2>
          </div>
          <p className="mt-8 opacity-60">
            Watch and Listen to our beautiful collection of concert snippets and
            gospel videos on our YouTube Channel now.
          </p>
          <motion.button
            className="bg-yellow-50 text-yellow-900 py-2 px-5 uppercase rounded-full mt-4"
            whileHover={{ scale: 1.1 }}
            whileTap={{ scale: 0.9 }}
          >
            youtube
          </motion.button>
        </SmallScreenHeroCTAContainer>
        <MobilePlayerContainer>
          <Title className="text-yellow-900">Latest Album</Title>
          <MobileMusicPlayer>
            <blockquote className="text-justify text-yellow-800 italic opacity-60 border-l border-yellow-900 my-8 pl-4">
              <b>Album Name:</b> Ewe Mwanadamu <br />
              <b>Songs:</b> Dunia hii, Ewe Bwana Mungu Wangu, Ewe Mwanadamu,
              Habari za Kurudi Kwake, Akuna Aliye Kama Yesu, Majibu ya Maombi,
              Musa, Mwana Mpotevu, Ndugu Zangu, Pendo La Yesu, Tazama Anakuja
            </blockquote>
            <iframe
              src="https://audiomack.com/embed/album/tumainishangilieni/ewe-mwanadamu?background=0&color=78350F"
              scrolling="no"
              width="100%"
              height="400"
              scrollbars="no"
              frameBorder="0"
            ></iframe>
          </MobileMusicPlayer>
        </MobilePlayerContainer>
      </SmallScreenContainer>
    </MainContainer>
  );
}
