import tw, { styled } from 'twin.macro';
// Hero section container
export const MainContainer = tw.section`
  
`;

// Hero Container for LargeScreen
export const LargeScreenContainer = tw.div`
  hidden
  md:block
  relative
`;

// Hero Content Container
export const HeroContentContainer = styled.div`
  ${tw`md:flex h-auto`}
`;

export const LargeScreenHeroCTAContainer = styled.div`
  ${tw`bg-yellow-50 w-4/6 pt-64 pb-52 text-yellow-900 md:px-6 lg:px-4 lg:px-24`}
`;

export const Player = styled.div`
  ${tw`relative w-8/12 lg:w-7/12 xl:w-5/12 top-28 -right-1/4 lg:-right-1/3 xl:-right-80 `}

  :before {
    content: '';
    position: absolute;
    right: -20px;
    top: 14px;
    border: none;
    border-top: 2px solid #fefbeb;
    border-right: 2px solid #fefbeb;
    border-radius: 4px;
    height: 4rem;
    width: 4rem;
  }

  :after {
    content: '';
    position: absolute;
    left: -20px;
    bottom: -20px;
    border: none;
    border-bottom: 2px solid #fefbeb;
    border-left: 2px solid #fefbeb;
    border-radius: 4px;
    height: 4rem;
    width: 4rem;
  }
`;

export const CoverImage = styled.div`
  ${tw`absolute left-10 md:w-7/12 xl:w-5/12 top-44`}
`;
// Hero Container for small screen
export const SmallScreenContainer = tw.div`
md:hidden
`;

export const SmallScreenHeroCTAContainer = styled.div`
  ${tw`pb-20 text-yellow-50 pt-32 px-8 bg-yellow-900`}
`;

export const MobilePlayerContainer = styled.div`
  ${tw`bg-yellow-50 p-2 pt-10 h-auto text-center`}
`;

export const MobileMusicPlayer = styled.div`
  ${tw`w-11/12 mx-auto h-full pb-6`}
`;
