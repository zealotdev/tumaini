import Head from 'next/head';
import { Hero, Hint, Library, Events, Products, Footer } from '../components';
import {
  motion,
  useViewportScroll,
  useSpring,
  useTransform,
} from 'framer-motion';
import { useState, useEffect } from 'react';

export const CircleIndicator = () => {
  const [isComplete, setIsComplete] = useState(false);
  const { scrollYProgress } = useViewportScroll();
  const yRange = useTransform(scrollYProgress, [0, 0.9], [0, 1]);
  const pathLength = useSpring(yRange, { stiffness: 400, damping: 90 });

  useEffect(() => yRange.onChange((v) => setIsComplete(v >= 1)), [yRange]);

  return (
    <svg viewBox="0 0 60 60">
      <motion.path
        fill="none"
        strokeWidth="5"
        stroke="#312626"
        strokeDasharray="0 1"
        d="M 0, 20 a 20, 20 0 1,0 40,0 a 20, 20 0 1,0 -40,0"
        style={{
          pathLength,
          rotate: 90,
          translateX: 5,
          translateY: 5,
          scaleX: -1, // Reverse direction of line animation
        }}
      />
      <motion.path
        fill="none"
        strokeWidth="5"
        stroke="#312626"
        d="M14,26 L 22,33 L 35,16"
        initial={false}
        strokeDasharray="0 1"
        animate={{ pathLength: isComplete ? 1 : 0 }}
      />
    </svg>
  );
};
export default function Home() {
  return (
    <div>
      <a
        href="#"
        className="fixed right-2 bottom-5 h-10 w-10 md:h-20 md:w-20 z-20"
      >
        <CircleIndicator />
      </a>
      <Head>
        <title>Tumaini | Gospel Choir</title>
        <link rel="icon" href="/logo.png" />
      </Head>
      <div>
        <Hero />
        <Hint />
        <Library />
        <Events />
        <Products />
        <Footer />
      </div>
    </div>
  );
}
